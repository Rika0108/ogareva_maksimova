// note_routes.js
var ObjectID = require('mongodb').ObjectID;
module.exports = function(app, db) {
	app.get('/get_all_soft', (req, res) => {  
		db.collection('soft').find({}).toArray(function(error, items) {
			if (error) {
				res.send({'error': 'Error! I can\'t select all soft!'});
				throw error;
			} 
			res.send(items);
		});
	});
	app.get('/get_soft/:id', (req, res) => {  
		const id = req.params.id;
		const details = { '_id': new ObjectID(id) };
		db.collection('soft').findOne(details, (err, item) => {
			if (err) {
				res.send({'error': 'Error! I can\'t select this soft!'});
				throw error;
			} 
			res.send(item);
		});
	});
	app.post('/set_soft', (req, res) => {
		const new_soft = { 
			title: 			req.body.title, 
			description: 	req.body.description, 
			icon: 			req.body.icon, 
			url: 			req.body.url 
		}; 
		db.collection('soft').insertOne(new_soft, (err, result) => {
			if (err) { 
				res.send({ 'error': 'Error! I can\'t insert this soft!' }); 
				throw error;
			} 
			res.send(result.ops[0]);
		});
	});
};